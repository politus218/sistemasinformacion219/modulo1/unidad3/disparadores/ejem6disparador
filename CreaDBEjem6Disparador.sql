﻿-- Vamos a crear una BD para usar con el ejemplo 6 de disparadores.

CREATE DATABASE IF NOT EXISTS disparadores_ejem6;
  USE disparadores_ejem6;

CREATE OR REPLACE TABLE productos(
  producto varchar(10) NOT NULL PRIMARY KEY,
  cantidad int(11) DEFAULT 0     
                                 )

ENGINE = INNODB,
character SET LATIN1,
COLLATE latin1_swedish_ci,
ROW_FORMAT = compact,
TRANSACTIONAL = 0;



CREATE OR REPLACE TABLE ventas (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  producto varchar(10) DEFAULT NULL,
  precio int(11) NOT NULL,
  unidades int(11) NOT NULL,
  total int(11) NOT NULL 
      
                               )

ENGINE = INNODB,
character SET LATIN1,
COLLATE latin1_swedish_ci,
ROW_FORMAT = compact,
TRANSACTIONAL = 0;         