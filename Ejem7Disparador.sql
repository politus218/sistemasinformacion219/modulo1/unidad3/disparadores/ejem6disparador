﻿-- Ejemplo 7 Disparadores y errores.

-- Modificamos las tablas clientes y provincias.
  
  USE ejemplo7;

  ALTER TABLE clientes ADD COLUMN edad int DEFAULT 0;
  ALTER TABLE provincias ADD COLUMN cantidad int DEFAULT 0;
  ALTER TABLE provincias ADD COLUMN iniciales char(1);   

-- Ejercicio 1.
-- Crear un disparador para que cuando inserte un resgistro en clientes me
-- compruebe si la edad está entre 18 y 70. En caso de que no esté, produz-
-- ca una excepción con el mensaje "La edad no es válida"

  DELIMITER //
  CREATE OR REPLACE TRIGGER clientesBI
  BEFORE INSERT 
  ON clientes
    FOR EACH ROW
  BEGIN 
    IF(new.edad NOT BETWEEN 18 AND 70) THEN
      SIGNAL SQLSTATE '4500' set
    MESSAGE_TEXT = "La edad no es válida";
    END IF;
  END     //
  DELIMITER;
