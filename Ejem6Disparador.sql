﻿ -- Ejemplo 6. Disparadores
-- Ejercicio 1.
-- Crear un disparador para la tabla ventas para que cuando metas un registro nuevo te calcule el total automáticamente.
-- El total debe ser unidades por precio.

USE disparadores_ejem6;

DELIMITER //
    CREATE OR REPLACE TRIGGER ventasBI
    BEFORE INSERT 
      ON ventas
      FOR EACH ROW
    BEGIN
    SET new.total=new.unidades*new.precio;
    END     //
DELIMITER;
           
  INSERT INTO ventas (unidades)
    VALUES ('1');
  SELECT * FROM ventas;

-- Ejercicio 2.

-- Crear un disparador para la tabla ventas para que cuando actualice un registro me sume el total a la tabla
-- productos (en el campo cantidad).

DELIMITER //
    CREATE OR REPLACE TRIGGER ventasAU
    AFTER UPDATE 
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos
        SET cantidad=cantidad+(NEW.total-OLD.total)
        WHERE producto=new.producto;
    END  //
DELIMITER;


-- Ejercicio 3. Crear un disparador para la tabla ventas para que cuando 
-- actualices un registro nuevo te calcule el total automáticamente.

DELIMITER //
    CREATE OR REPLACE TRIGGER ventasBU
      BEFORE UPDATE
      ON ventas
      FOR EACH ROW
      BEGIN
        SET new.total=new.unidades*new.precio;
        UPDATE productos
          SET cantidad=cantidad+(new.total-old.total)
          WHERE producto=new.producto;
      END  //
DELIMITER;

-- Ejercicio 4. Crear un disparador para la tabla ventas, para que cuando
-- actualice un registro me sume el total a la tabla productos(en el campo
-- cantidad)

DELIMITER //
    CREATE OR REPLACE TRIGGER ventasBU
      BEFORE UPDATE 
      ON ventas
      FOR EACH ROW
      BEGIN
      UPDATE productos
        SET cantidad=cantidad+(NEW.total-OLD.total)
        WHERE producto=NEW.producto;
      END //
DELIMITER;

-- Ejercicio 5. Crear un disparador para la tabla productos que si cambia
-- el código del producto te sume todos los totales de ese producto de la
-- tabla ventas.

DELIMITER //
    CREATE OR REPLACE TRIGGER productosBU
      BEFORE UPDATE 
      ON productos
      FOR EACH ROW
      BEGIN
        SET NEW.cantidad=(
              SELECT SUM(v.total) FROM ventas
                WHERE producto=NEW.producto
                         );
      END //
DELIMITER;

-- Ejercicio 6. Crear un disparador para la tabla productos que si eliminas
-- un producto te elimine todos los productos del mismo código en la tabla
-- ventas.

DELIMITER //
  CREATE OR REPLACE TRIGGER productosAD
    AFTER DELETE 
    ON productos
    FOR EACH ROW
    BEGIN
      DELETE FROM ventas
      WHERE producto=OLD.producto;
    END //
DELIMITER;

-- Ejercicio 7. Crear un disparador para la tabla ventas que si eliminas
-- un registro te reste el total del campo cantidad de la tabla productos
-- (en el campo cantidad).

DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAD
    AFTER DELETE 
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos
      SET cantidad=cantidad-old.total
      WHERE producto=old.producto;
    END //
DELIMITER;





