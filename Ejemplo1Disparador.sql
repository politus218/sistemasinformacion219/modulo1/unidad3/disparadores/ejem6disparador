﻿
-- Ejemplo disparador(trigger) con la base de datos ejemplotrigger.

CREATE DATABASE IF NOT EXISTS ejemplotrigger;
  USE ejemplotrigger;

CREATE OR REPLACE TABLE local(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre_local varchar(100),
  precio float,
  plazas int                 );

CREATE OR REPLACE TABLE usuario (

id int AUTO_INCREMENT PRIMARY KEY,
nombre_usuario varchar(100),
Inicial varchar (100)
                                );

CREATE OR REPLACE TABLE usa (

local_id int AUTO_INCREMENT PRIMARY KEY,
usuario_local varchar(50),
dias int,
fecha date,
total float
                            );

-- Queremos que cuando introduzca un usuario me calcule y almacene la inicial de su nombre. 
  
    DELIMITER //
    CREATE OR REPLACE TRIGGER usuarioBI
    BEFORE INSERT -- Antes del insert no hay dato alguno.
      ON usuario
      FOR EACH ROW
    BEGIN
    SET new.Inicial=LEFT(new.nombre_usuario,1); -- Con el set ha de ir siempre un new porque hemos puesto un insert.
    END     //
    DELIMITER;
           
  INSERT INTO usuario (nombre_usuario)
    VALUES ('Roberto');
  SELECT * FROM usuario;

-- Realizamos algo parecido con el disparador pero sobre toda la tabla.

  UPDATE usuario
    SET inicial=LEFT(nombre,1);

  TRUNCATE usuario;
  INSERT INTO usuario (nombre)
    VALUES ('Roberto'),('kevin'),('Luis');
  SELECT * FROM usuario;

-- Modificamos la tabla, añadiendo un contador.

  ALTER TABLE usa
    ADD COLUMN contador int DEFAULT 0;

-- Cuando inserto un usuario a la tabla usa me debe sumar a contador un 1.

DELIMITER //
CREATE OR REPLACE TRIGGER usaA1
  AFTER INSERT ON usa
  FOR EACH ROW
BEGIN
  UPDATE usuario
  SET contador=contador+1
  WHERE id=new.usuario_local;
  END //
DELIMITER;

TRUNCATE usa;
  INSERT INTO usa (local, usuario_local)
    VALUES (1,2),(2,1),(3,2);
  SELECT * FROM usuario;
  SELECT * FROM usa;

-- Consulta de actualización que realice lo que hace el disparador anterior.

-- Me tiene que colocar en contador las veces que ese usuario sale en la tabla
-- usa.

-- c1

SELECT usuario_local, COUNT(*) FROM usuario JOIN usa ON usuario_local=ID
  GROUP BY usuario_local;

-- optimizada

SELECT usuario_local, COUNT(*) FROM usa GROUP BY usuario_local;

--

UPDATE usuario JOIN (
  SELECT usuario, COUNT(*) numero FROM usa 
   GROUP BY usuario_local);
  -- c1
  ON c1.usuario=id
SET contador = numero;
  UPDATE usuario
    set contador=(
    SELECT COUNT(*) FROM usa WHERE usuario=ID
                 );
   SELECT * FROM usuario;
   SELECT * FROM usa;

-- Otro disparador, para calcular el total de usa en función del precio del local, cuando introducimos
-- un dato nuevo en usa.

DELIMITER //
  CREATE OR REPLACE TRIGGER usaBI
  BEFORE INSERT
  ON usa
  FOR EACH ROW
  BEGIN
  set new.total=new.dias*(
    SELECT precio FROM local WHERE id=new.local
                         );
END //
DELIMITER;

  TRUNCATE local;
  INSERT INTO local (nombre, precio)
    VALUES('11',10), ('12' , 5), ('13' ,7);
  TRUNCATE usa;
  INSERT INTO usa (local, usuario, dias)
    VALUES (1, 1, 5), (2,1,7),(1,3,80);


